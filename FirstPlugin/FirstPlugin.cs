﻿using PluginContracts;
using PluginPattern;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace FirstPlugin
{
    public class FirstPlugin : IPlugin
    {
        public string Name
        {
            get
            {
                return "FirstPlugin";
            }
        }

        private string[] keywords = new string[]
        {
            "public", "private", "get", "set"
        };

        private FontWeight fontWeight = FontWeights.Bold;
        private Brush foreground = Brushes.Blue;



        public void Do(RichTextBox richTextBox)
        {
            //richTextBox.SelectAll();
            //richTextBox.Selection.ClearAllProperties();
            TextPointer start = richTextBox.Document.ContentStart;
            TextPointer end = richTextBox.Document.ContentEnd;

            foreach (string keyword in keywords)
            {
                TextFormatter.Format(start, end, keyword, fontWeight, foreground);
            }
        }
    }

}
