﻿using Microsoft.Build.Evaluation;
using Microsoft.Build.Framework;
using Microsoft.Build.Logging;
using PluginContracts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Visual_Student
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private string projectPath;
        private string projectName;

        private string output;
        private ObservableCollection<ErrorLog> errors;

        private ObservableCollection<FileModel> files;
        private ObservableCollection<TreeItem> folders;

        private ObservableCollection<IPlugin> plugins;

        public string OutPut
        {
            get
            {
                return output;
            }
            set
            {
                this.output = value;
                OnPropertyChanged("OutPut");
            }
        }

        public ObservableCollection<ErrorLog> Errors
        {
            get
            {
                return errors;
            }
            set
            {
                this.errors = value;
                OnPropertyChanged("Errors");
            }
        }


        public ObservableCollection<FileModel> Files
        {
            get
            {
                return files;
            }
            set
            {
                files = value;
                OnPropertyChanged("Files");
            }
        }

        public ObservableCollection<TreeItem> Folders
        {
            get
            {
                return folders;
            }
            set
            {
                folders = value;
                OnPropertyChanged("Folders");
            }
        }

        public ObservableCollection<IPlugin> Plugins
        {
            get
            {
                return plugins;
            }
            set
            {
                plugins = value;
                OnPropertyChanged("Plugins");
            }
        }

        public MainViewModel()
        {
            var files = new ObservableCollection<FileModel>();
            var folders = new ObservableCollection<TreeItem>();
            Files = files;
            Folders = folders;

            try
            {
                var plugins = LibraryLoader.LoadPlugins();
                Plugins = new ObservableCollection<IPlugin>(plugins);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

        }

        public bool NewFile()
        {
            FileModel newFile = new FileModel("New file");
            files.Add(newFile);
            return true;
        }

        public bool OpenFile()
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog
            {
                DefaultExt = ".cs",
                Filter = "C# files (.cs) | *.cs"
            };

            var result = dialog.ShowDialog();

            if (result == true)
            {
                string filename = dialog.FileName;
                try
                {
                    using (StreamReader reader = new StreamReader(filename))
                    {
                        string content = reader.ReadToEnd();
                        FileModel openedFile = new FileModel(filename, content);
                        Files.Add(openedFile);
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public TreeItem OpenProject()
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    string[] files = Directory.GetFiles(dialog.SelectedPath);
                    bool valid = false;
                    foreach (string file in files)
                    {
                        if (file.EndsWith(".csproj"))
                        {
                            valid = true;
                            projectPath = file;
                            projectName = Path.GetFileNameWithoutExtension(file);
                            break;
                        }
                    }
                    if (valid)
                    {
                        return new TreeItem(dialog.SelectedPath);
                    }
                    else
                    {
                        MessageBox.Show("This is not proper folder with C# project.", "An error occured",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            return null;
        }

        public bool OpenProjectFile(TreeItem item)
        {
            if (!Directory.Exists(item.Path))
            {
                try
                {
                    using (StreamReader reader = new StreamReader(item.Path))
                    {
                        string content = reader.ReadToEnd();
                        Files.Add(new FileModel(item.Path, content));
                        return true;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Error during reading file", "An error occured",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            return false;
        }

        public bool CloseFile(FileModel file)
        {
            int index = Files.IndexOf(file);
            if (file.Changed)
            {
                var result = MessageBox.Show("Do you want to close unsaved document?", "Close document",
                    MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No)
                {
                    return false;
                }
            }
            try
            {
                Files.RemoveAt(index);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }

        public void SaveFile(FileModel currentFile)
        {
            System.Windows.Forms.SaveFileDialog saveDialog = new System.Windows.Forms.SaveFileDialog();
            saveDialog.FileName = currentFile.Name;
            saveDialog.Filter = "C# files (*.cs)|*.cs";
            saveDialog.RestoreDirectory = true;

            if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllText(saveDialog.FileName, currentFile.Content);

                currentFile.Changed = false;
                currentFile.Name = Path.GetFileName(saveDialog.FileName);

            }
        }

        public void Build(string option)
        {
            if (projectPath == null) return;
            if (!File.Exists(projectPath)) return;


            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                var p = new Project(projectPath);
                p.SetProperty("Configuration", "Debug");

                ConsoleLogger consoleLogger =
                    new ConsoleLogger(LoggerVerbosity.Normal,
                    s => stringBuilder.Append(s), null, null);

                ErrorLogger errorLogger = new ErrorLogger();

                List<ILogger> loggers = new List<ILogger>
                {
                    consoleLogger,
                    errorLogger
                };

                p.Build(loggers);

                ProjectCollection.GlobalProjectCollection.UnloadAllProjects();
                OutPut = stringBuilder.ToString();
                Errors = errorLogger.Errors;

                if (option.Equals("Build + Run"))
                {
                    string exe = Path.Combine(new FileInfo(projectPath).Directory.FullName, "bin", "Debug", projectName + ".exe");
                    Process myProcess = new Process();
                    myProcess.StartInfo.UseShellExecute = false;
                    myProcess.StartInfo.FileName = exe;
                    myProcess.StartInfo.CreateNoWindow = false;
                    myProcess.Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
