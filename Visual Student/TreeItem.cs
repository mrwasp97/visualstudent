﻿using System.Collections.ObjectModel;
using System.IO;

namespace Visual_Student
{
    public class TreeItem
    {
        public string Path { get; set; }
        public string Title { get; set; }
        public ObservableCollection<TreeItem> Folders { get; set; }

        public TreeItem(string path)
        {
            Path = path;
            Title = System.IO.Path.GetFileName(path);
            Folders = new ObservableCollection<TreeItem>();

            if (Directory.Exists(path))
            {
                string[] files = Directory.GetFiles(path);
                foreach (string file in files)
                {
                    if (file.EndsWith(".cs"))
                    {
                        Folders.Add(new TreeItem(file));
                    }
                }
                string[] dirs = Directory.GetDirectories(path);
                foreach (string dir in dirs)
                {
                    Folders.Add(new TreeItem(dir));
                }
            }
        }
    }
}
