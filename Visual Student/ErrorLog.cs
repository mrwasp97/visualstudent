﻿using System.ComponentModel;

namespace Visual_Student
{
    public class ErrorLog : INotifyPropertyChanged
    {
        private string code;
        private string message;
        private string file;

        public string Code
        {
            get
            {
                return code;
            }
            set
            {
                this.code = value;
                OnPropertyChanged("Code");
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                this.message = value;
                OnPropertyChanged("Message");
            }
        }

        public string File
        {
            get
            {
                return file;
            }
            set
            {
                this.file = value;
                OnPropertyChanged("File");
            }
        }

        public ErrorLog(string code, string message, string file)
        {
            this.Code = code;
            this.Message = message;
            this.File = file;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
