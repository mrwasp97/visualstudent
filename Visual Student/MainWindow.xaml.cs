﻿using Microsoft.Build.Evaluation;
using Microsoft.Build.Execution;
using Microsoft.Build.Framework;
using Microsoft.Build.Logging;
using Microsoft.Build.Utilities;
using PluginContracts;
using PluginPattern;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Visual_Student
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<IPlugin> activePlugins;

        private MainViewModel viewModel;
        public static RoutedCommand MyCommand = new RoutedCommand();

        public MainWindow()
        {
            viewModel = new MainViewModel();
            this.DataContext = viewModel;
            this.activePlugins = new List<IPlugin>();
            InitializeComponent();
            MyCommand.InputGestures.Add(new KeyGesture(Key.F5));
        }

        private void About(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("This is simple C# editor and compiler.", "About",
                MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void NewFile(object sender, RoutedEventArgs e)
        {
            if (viewModel.NewFile())
            {
                MyTabControl.SelectedIndex = viewModel.Files.Count - 1;
                viewModel.Files[viewModel.Files.Count - 1].Changed = false;
                viewModel.Files[viewModel.Files.Count - 1].First = true;
            }
        }

        private void OpenFile(object sender, RoutedEventArgs e)
        {
            if (viewModel.OpenFile())
            {
                MyTabControl.SelectedIndex = viewModel.Files.Count - 1;
                viewModel.Files[viewModel.Files.Count - 1].Changed = false;
            }
        }

        private void OpenProject(object sender, RoutedEventArgs e)
        {
            TreeItem res = viewModel.OpenProject();
            if (res != null)
            {
                MyTree.Items.Add(res);
            }
        }

        private void TreeItemClick(object sender, MouseButtonEventArgs e)
        {
            TreeViewItem viewItem = sender as TreeViewItem;
            TreeItem item = viewItem.Header as TreeItem;
            if (viewModel.OpenProjectFile(item))
            {
                MyTabControl.SelectedIndex = viewModel.Files.Count - 1;
            }
            e.Handled = true;
        }

        private void CloseFile(object sender, RoutedEventArgs e)
        {
            FileModel file = (FileModel)(sender as FrameworkElement).DataContext;
            if (viewModel.CloseFile(file))
            {
                MyTabControl.SelectedIndex = viewModel.Files.Count - 1;
            }
        }

        private void SaveFile(object sender, ExecutedRoutedEventArgs e)
        {
            FileModel currentFile = ((FileModel)MyTabControl.SelectedItem);
            RichTextBox richTextBox = RetrieveTextBox(MyTabControl);
            string text = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd).Text;
            currentFile.Content = text;
            viewModel.SaveFile(currentFile);
        }

        private void SaveFileAs(object sender, ExecutedRoutedEventArgs e)
        {
            FileModel currentFile = ((FileModel)MyTabControl.SelectedItem);
            RichTextBox richTextBox = RetrieveTextBox(MyTabControl);
            string text = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd).Text;
            currentFile.Content = text;
            viewModel.SaveFile(currentFile);
        }

        private void Build(object sender, RoutedEventArgs e)
        {
            string option = (string)MyComboBox.SelectionBoxItem;
            viewModel.Build(option);
        }

        private void Build2()
        {
            string option = (string)MyComboBox.SelectionBoxItem;
            viewModel.Build(option);
        }

        private void TabControlSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (MyTabControl.SelectedIndex >= 0)
                {
                    RichTextBox richTextBox = RetrieveTextBox((TabControl)sender);
                    RefreshTextBox(richTextBox);
                    ApplyPlugins(richTextBox);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void RefreshTextBox(RichTextBox richTextBox)
        {
            FileModel file = (FileModel)MyTabControl.SelectedItem;
            bool changed = file.Changed;
            Paragraph paragraph = new Paragraph(new Run(file.Content));
            richTextBox.Document.Blocks.Clear();
            richTextBox.Document.Blocks.Add(paragraph);
            file.Changed = changed;
        }

        private RichTextBox RetrieveTextBox(TabControl tabControl)
        {
            DataTemplate template = tabControl.ContentTemplate;
            var contentPresenter = (ContentPresenter)tabControl.Template.FindName("PART_SelectedContentHost", tabControl);
            contentPresenter.ApplyTemplate();
            return (RichTextBox)template.FindName("MyRichTextBox", contentPresenter);
        }

        private void RichTextBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            RichTextBox richTextBox = (RichTextBox)sender;
            FileModel currentFile = ((FileModel)MyTabControl.SelectedItem);
            if (currentFile.First)
            {
                currentFile.First = false;
            }
            else
            {
                currentFile.Changed = true;
            }
        }

        private void ApplyPlugins(RichTextBox richTextBox)
        {
            FileModel file = (FileModel)MyTabControl.SelectedItem;
            bool changed = file.Changed;
            foreach (IPlugin plugin in activePlugins)
            {
                plugin.Do(richTextBox);
            }
            TextSelection selection = richTextBox.Selection;
            if (selection != null)
            {
                selection.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Regular);
                selection.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.Black);
            }
            file.Changed = changed;
        }

        private void CheckPlugin(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = (MenuItem)sender;
            IPlugin plugin = (IPlugin)menuItem.DataContext;
            activePlugins.Add(plugin);
            if (viewModel.Files.Count > 0)
            {
                RichTextBox richTextBox = RetrieveTextBox(MyTabControl);
                ApplyPlugins(richTextBox);
            }
        }

        private void UncheckPlugin(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = (MenuItem)sender;
            IPlugin plugin = (IPlugin)menuItem.DataContext;
            activePlugins.Remove(plugin);
            if (viewModel.Files.Count > 0)
            {
                RichTextBox richTextBox = RetrieveTextBox(MyTabControl);
                FileModel currentFile = ((FileModel)MyTabControl.SelectedItem);
                string text = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd).Text;
                currentFile.Content = text;
                RefreshTextBox(richTextBox);
                ApplyPlugins(richTextBox);
            }
        }

        private void MyRichTextBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            ApplyPlugins((sender as RichTextBox));
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Build2();
        }
    }
}
