﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Data;

namespace Visual_Student
{
    public class FileModel : INotifyPropertyChanged
    {
        private string path;
        private string name;
        private string content;
        private bool changed;
        private bool first;

        public string Path
        {
            get
            {
                return path;
            }
            set
            {
                this.path = value;
                OnPropertyChanged("Path");
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                this.name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Content
        {
            get
            {
                return content;
            }
            set
            {
                this.content = value;
                OnPropertyChanged("Content");
            }
        }

        public bool Changed
        {
            get
            {
                return changed;
            }
            set
            {
                this.changed = value;
                OnPropertyChanged("Changed");
            }
        }

        public bool First
        {
            get
            {
                return first;
            }
            set
            {
                this.first = value;
            }
        }

        public FileModel(string path, string content = "")
        {
            Path = path;
            Name = System.IO.Path.GetFileName(path);
            Content = content;
            Changed = false;
            first = false ;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }

    public class ChangedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool changed = (bool)value;
            return changed ? "*" : "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
