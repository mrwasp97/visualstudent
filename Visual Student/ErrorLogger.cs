﻿using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections.ObjectModel;

namespace Visual_Student
{
    public class ErrorLogger : Logger
    {
        private ObservableCollection<ErrorLog> errors;

        public ErrorLogger()
        {
            errors = new ObservableCollection<ErrorLog>();
        }
        public override void Initialize(IEventSource eventSource)
        {
            eventSource.ErrorRaised += LogErrorFunction;
        }

        private void LogErrorFunction(object sender, BuildErrorEventArgs e)
        {
            ErrorLog log = new ErrorLog("error " + e.Code, e.Message, e.File);
            errors.Add(log);
        }

        public ObservableCollection<ErrorLog> Errors
        {
            get
            {
                return errors;
            }
        }
    }
}
