﻿using PluginContracts;
using PluginPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace SecondPlugin
{
    public class SecondPlugin : IPlugin
    {
        public string Name
        {
            get
            {
                return "SecondPlugin";
            }
        }

        private string[] keywords = new string[]
        {
            "using", "if", "else", "foreach", "for"
        };

        private FontWeight fontWeight = FontWeights.Bold;
        private Brush foreground = Brushes.Red;



        public void Do(RichTextBox richTextBox)
        {
            TextPointer start = richTextBox.Document.ContentStart;
            TextPointer end = richTextBox.Document.ContentEnd;

            foreach (string keyword in keywords)
            {
                TextFormatter.Format(start, end, keyword, fontWeight, foreground);
            }
        }
    }
}
